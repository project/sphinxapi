<?php

function sphinxapi_admin_form($form_state) {
  $form = array();
  $form['sphinxapi_indexes'] = array(
    '#type' => 'select',
    '#title' => t('Sphinx indexes'),
    '#description' => t('Choose the Sphinx indexes to search in.'),
    '#default_value' => variable_get('sphinxapi_indexes', SPHINXAPI_INDEXES),
  );
  $form['sphinxapi_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Sphinx host'),
    '#description' => t('Enter the address of Sphinx server.'),
    '#default_value' => variable_get('sphinxapi_host', SPHINXAPI_HOST),
  );
  $form['sphinxapi_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Sphinx host'),
    '#description' => t('Enter the address of Sphinx server.'),
    '#default_value' => variable_get('sphinxapi_port', SPHINXAPI_PORT),
  );
  return drupal_settings_form($form);
}

function sphinxapi_admin_form($form, &$form_state) {
  // port size validate. 0 <= port <= 65535. RFC #
}
